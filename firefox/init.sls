firefox:
  25.0:
    installer: 'https://download.mozilla.org/?product=firefox-25.0&os=win&lang=en-US'
    full_name: Mozilla Firefox 25.0 (x86 en-US)
    locale: en_US
    reboot: False
    install_flags: ' -ms'
    uninstaller: '%ProgramFiles(x86)%/Mozilla Firefox/uninstall/helper.exe'
    uninstall_flags: ' /S'

