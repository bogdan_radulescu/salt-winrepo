7zip:
  9.22:
    installer: 'http://cdn.mirror.garr.it/sf/project/sevenzip/7-Zip/9.22/7z922-x64.msi'
    full_name: 7-Zip 9.22 (x64 edition)
    reboot: False
    install_flags: ' /q '
    msiexec: True
    uninstaller: 'http://cdn.mirror.garr.it/sf/project/sevenzip/7-Zip/9.22/7z922-x64.msi'
    uninstall_flags: ' /qn'

